Chrome Store Installation Link:  **SOON**

Firefox Store Installation Link: **SOON**
## Developer Installation Guide:
1. Open extensions page: (url may vary for browsers)
* chrome://extensions (Chrome, Chromium and others chromium based browsers)
* opera://extensions (Opera, yeah its the best browser in my opinion /comment by mcrdy455/)
* browser://extensions (Yandex and many more)
* about:debugging#/runtime/this-firefox (Firefox)

2. Enable Developer Mode

3. Click "Load Unpacked"
* Select the extension folder
* For Firefox you have to select the manifest.json file

4. You can now test/use the extension. If you change any of the files you have to reload the extension from the extensions page

**NOTE!** For firefox you'll have to repeat the process every time you restart your browser or have to use a .XPI file for installation

## How to use:
Simply click the extension browser action - The icon in the upper right corner of the browser and the scripts will be added to the console.


## Contents:
> list of functions:

* capitalizeFirstLetter(string)
* copyStringToClipboard (string)
* getXpathNodesArray (path)
* getElementByXpath (path) 
* copyStringToClipboard (string)
* randomNum(min, max)
* randomNumArr(length = 10, min = 0, max = 100, toFixed = 0) 
* getElementWithinView(arrayOfElements)
* createElementFromHTML(htmlString)
* getNiceDate(date = new Date())
* getNiceTime(date = new Date()) 
* fakeClick(el)
* randomString(length)
* mouseOver(el)
* removeDuplicates(myArr, prop)
* findParentBySelector(elm, selector)
* findParentByClass(elm,className)
> list of classes:
* EventObject //used for MouseEvent, KeyboardEvent 
